#!/bin/bash

## Initialisation des règles
iptables -t filter -F
iptables -t filter -X
iptables -t nat -F

## politique appliquée DROP on ne laisse rien passer
iptables -P FORWARD DROP
iptables -P INPUT DROP
iptables -P OUTPUT DROP

## Utilisation du suivi de connexion
#iptables -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A INPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -A FORWARD -m state --state RELATED,ESTABLISHED -j ACCEPT
iptables -t filter -A INPUT -i lo -j ACCEPT
iptables -t filter -A OUTPUT -o lo -j ACCEPT
iptables -t filter -A FORWARD -o lo -j ACCEPT


iptables -t filter -A INTPUT -p tcp --dport 80 -j ACCEPT
iptables -t filter -A INTPUT -p tcp --dport 443 -j ACCEPT
iptables -t filter -A OUTPUT -p tcp --dport 80 -j ACCEPT
iptables -t filter -A OUTPUT -p tcp --dport 443 -j ACCEPT
iptables -t filter -A FORWARD -p tcp --dport 80 -j ACCEPT
iptables -t filter -A FORWARD -p tcp --dport 443 -j ACCEPT

## possibilité de se connecter au serveur web deputs le firewall 
iptables -A INPUT -m state --state NEW -s 192.168.16.254 -d 192.168.16.1 -p tcp --dport 22 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -s 192.168.16.254 -d 192.168.16.1 -p tcp --dport 22 -j ACCEPT

## Connexion de l'admin au firewall
iptables -A INPUT -m state --state NEW -s 192.168.9.1 -d 192.168.9.254 -p tcp --dport 22 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -s 192.168.9.1 -d 192.168.9.254 -p tcp --dport 22 -j ACCEPT

## DNS in/out
iptables -A INPUT -m state --state NEW -p udp --dport 53 -j ACCEPT
iptables -A INPUT -m state --state NEW -p tcp --dport 53 -j ACCEPT
iptables -A FORWARD -m state --state NEW -p tcp --dport 53 -j ACCEPT
iptables -A FORWARD -m state --state NEW -p udp --dport 53 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p tcp --dport 53 -j ACCEPT
iptables -A OUTPUT -m state --state NEW -p udp --dport 53 -j ACCEPT

##ICMP
#iptables -t filter -A OUTPUT -p icmp -j ACCEPT
#iptables -t filter -A INPUT -p icmp -j ACCEPT
iptables -t filter -A FORWARD -p icmp -j ACCEPT

SERVERIP="192.168.10.1"
## Paramètrage des ports de l'AD
iptables -A FORWARD -p tcp -m tcp --dport 53 -m comment --comment "Name Resolution Service" -j ACCEPT
iptables -A FORWARD -p udp -m udp --dport 53 -m comment --comment "Name Resolution Service" -j ACCEPT
iptables -A FORWARD -p udp -m udp --dport 42 -m comment --comment WINS -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 42 -m comment --comment WINS -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 137 -m comment --comment "Name Resolution Service" -j ACCEPT
iptables -A FORWARD -p udp -m udp --dport 137 -m comment --comment "Name Resolution Service" -j ACCEPT
iptables -A FORWARD -p udp -m udp --dport 138 -m comment --comment "Datagram  Services (Browsing)" -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 139 -m comment --comment "Session Service (net use)" -j ACCEPT
iptables -A FORWARD -p udp -m udp --dport 445 -m comment --comment SMB -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 445 -m comment --comment SMB -j ACCEPT
iptables -A FORWARD -p udp -m udp --dport 1025 -m comment --comment "Remote Storm" -j ACCEPT
iptables -A FORWARD -p udp -m udp --dport 123 -m comment --comment NTP -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 123 -m comment --comment NTP -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 507 -m comment --comment Content_Repl -j ACCEPT
iptables -A FORWARD -p udp -m udp --dport 750 -m comment --comment Kerberos_Secure -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 88 -m comment --comment Kerberos_v5 -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 464 -m comment --comment Kerberos_v5 -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 88 -m comment --comment Kerberos_v5 -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 464 -m comment --comment Kerberos_v5 -j ACCEPT
iptables -A FORWARD -p udp -m udp --dport 389 -m comment --comment LDAP -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 389 -m comment --comment LDAP -j ACCEPT
iptables -A FORWARD -p udp -m udp --dport 636 -m comment --comment LDAP -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 636 -m comment --comment LDAP -j ACCEPT
iptables -A FORWARD -p udp -m udp --dport 445 -m comment --comment "Microsoft-CIFS (DS) " -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 445 -m comment --comment "Microsoft-CIFS (DS) " -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 135 -m comment --comment RPC -j ACCEPT
iptables -A FORWARD -p udp -m udp --dport 161 -m comment --comment SNMP -j ACCEPT
iptables -A FORWARD -p tcp -m tcp --dport 162 -m comment --comment "SNMP TRAP" -j ACCEPT
iptables -A FORWARD -p udp -m udp --dport 42424 -m comment --comment "ASP.Net State Service" -j ACCEPT
iptables -A FORWARD -p udp -m udp --dport 691 -m comment --comment "Link State Algorithm Routing" -j ACCEPT
iptables -A FORWARD -p udp -m udp -m comment --comment Resto -j ACCEPT
iptables -A FORWARD -p tcp -m tcp -m comment --comment Resto -j ACCEPT
iptables -A FORWARD -p icmp -m icmp --icmp-type 8 -m comment --comment echo-reply -j ACCEPT
iptables -A FORWARD -p icmp -m icmp --icmp-type 0 -m comment --comment destination-unreachable -j ACCEPT
iptables -A FORWARD -p icmp -m icmp --icmp-type 3 -m comment --comment time-exceeded -j ACCEPT
iptables -A FORWARD -p icmp -m icmp --icmp-type 11 -m comment --comment echo-request -j ACCEPT


IPT=/sbin/iptables

$IPT  -t nat -A POSTROUTING -o enp0s3 -j MASQUERADE
route -n|grep 192.168.122.1
if [ $? -eq 0 ]
then
	echo "Déja inscrit dans la route"
else
    route add default gw 192.168.122.1
fi

 
exit 0

