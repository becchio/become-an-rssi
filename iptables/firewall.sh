#!/bin/bash
#
# firewall.sh

IPT=/sbin/iptables

$IPT  -t nat -A POSTROUTING -o enp0s3 -j MASQUERADE
route -n|grep 192.168.122.1
if [ $? -eq 0 ]
then
	echo "Déja inscrit dans la route"
else
    route add default gw 192.168.122.1
fi
