#!/bin/bash
 
iptables -F
iptables -X
iptables -t nat -F
iptables -t nat -X
iptables -t mangle -F
iptables -t mangle -X
 
## On récupère notre adresse internet. À utiliser seulement si vous êtes derrière un réseau local.
 
export ip=$(/sbin/ip addr | grep 'state UP' -A2 | tail -n1 | awk '{print $2}' | cut -f1  -d'/')
echo $ip

## Allow Samba.
 
iptables -t raw -A OUTPUT -p udp -m udp --dport 137 -j CT --helper netbios-ns
 
## Allow Avahi-daemon (seulement notre LAN. Si vous n'avez pas de LAN, supprimer la partie --source $ip/24).
 
iptables -A INPUT -p udp -m udp --source $ip/24  --dport 5353 -j ACCEPT
 
iptables -A INPUT -p udp -m udp --source $ip/24  --dport 427 -j ACCEPT
 
## On accepte le Multicast.
 
iptables -A INPUT -m pkttype --pkt-type multicast -j ACCEPT
 
## On drop tout le trafic entrant.
 
iptables -P INPUT DROP
 
## On drop tout le trafic sortant.
 
iptables -P OUTPUT DROP
 
## On drop le forward.
 
iptables -P FORWARD DROP
 
## On drop les scans XMAS et NULL.
 
iptables -A INPUT -p tcp --tcp-flags FIN,URG,PSH FIN,URG,PSH -j DROP
 
iptables -A INPUT -p tcp --tcp-flags ALL ALL -j DROP
 
iptables -A INPUT -p tcp --tcp-flags ALL NONE -j DROP
 
iptables -A INPUT -p tcp --tcp-flags SYN,RST SYN,RST -j DROP
 
## Dropper silencieusement tous les paquets broadcastés.
 
iptables -A INPUT -m pkttype --pkt-type broadcast -j DROP
 
## Permettre à une connexion ouverte de recevoir du trafic en entrée.
 
#iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
 
## Permettre à une connexion ouverte de recevoir du trafic en sortie.
 
#iptables -A OUTPUT -m conntrack ! --ctstate INVALID -j ACCEPT
 
## On accepte la boucle locale en entrée.
 
iptables -I INPUT -i lo -j ACCEPT
 
## On log les paquets en entrée.
 
iptables -A INPUT -j LOG
 
## On log les paquets forward.
 
iptables -A FORWARD -j LOG 

## On drope le port 22
iptables -A INPUT -p tcp --dport 22 -j DROP
iptables -A OUTPUT -p tcp --dport 22 -j DROP

## On permet la connexion ssh pour le firewall et le serveur web
iptables -i enp0s9 -A INPUT -p tcp --dport 22 -j ACCEPT
iptables -o enp0s9 -A OUTPUT -p tcp --dport 22 -j ACCEPT

## On autorise les pings
iptables -A INPUT -p icmp -j ACCEPT
iptables -A OUTPUT -p icmp -j ACCEPT

## On autorise le DNS
iptables -A INPUT -i enp0s3 -p udp --sport 53 -m state --state ESTABLISHED -j ACCEPT
iptables -A INPUT -i enp0s3 -p tcp --sport 53 -m state --state ESTABLISHED -j ACCEPT
iptables -A OUTPUT -o enp0s3 -p udp --dport 53 -m state --state NEW,ESTABLISHED -j ACCEPT
iptables -A OUTPUT -o enp0s3 -p tcp --dport 53 -m state --state NEW,ESTABLISHED -j ACCEPT

IPT=/sbin/iptables

$IPT  -t nat -A POSTROUTING -o enp0s3 -j MASQUERADE
route -n|grep 192.168.122.1
if [ $? -eq 0 ]
then
	echo "Déja inscrit dans la route"
else
    route add default gw 192.168.122.1
fi

 
exit 0

