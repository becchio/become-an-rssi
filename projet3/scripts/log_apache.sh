#!/bin/bash

su root
chown -R www-data:root /var/log/apache2/
chmod -R u=rwX /var/log/apache2/

chown -R www-data:root /etc/apache2
chmod -R u=rx /etc/apache2
