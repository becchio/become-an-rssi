#!/bin/bash

su - root
userdel -r webadmin
adduser webadmin
sudo usermod -G www-data -a webadmin

chown -R webadmin:www-data /var/www
chmod -R u=rwx,g=rx,o=X /var/www
